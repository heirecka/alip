# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Based in part upon bip-0.8.0_rc1.ebuild which is:
#   Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Multiuser IRC proxy with ssl support"
DESCRIPTION="
Bip is an IRC proxy, which means it keeps connected to your preferred IRC
servers, can store the logs for you, and even send them back to your IRC
client(s) upon connection. You may want to use bip to keep your logfiles (in a
unique format and on a unique computer) whatever your client is, when you
connect from multiple workstations, or when you simply want to have a playback
of what was said while you were away.
"
HOMEPAGE="http://bip.milkypond.org/"
DOWNLOADS=""
SCM_REPOSITORY="http://rcs-git.duckcorp.org/projects/bip/bip.git/"
require scm-git
BUGS_TO="alip@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="debug ssl vim-syntax"

DEPENDENCIES="
    build+run:
        ssl? ( dev-libs/openssl )
    run:
        vim-syntax? ( app-editors/vim )
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug ssl )

src_prepare() {
    default
    ./bootstrap || die "bootstrap failed"
}

src_compile() {
    # Parallel make fails
    # {C,CXX,LD}FLAGS aren't respected
    emake CFLAGS="${CFLAGS}" \
        CPPFLAGS="${CXXFLAGS}" \
        LDFLAGS="${LDFLAGS}" -j1 || die "emake failed"
}

src_install() {
    dobin src/${PN} src/bipmkpw

    newdoc samples/bip.conf bip.conf.sample
    doman bip.1 bip.conf.5 bipmkpw.1

    if option vim-syntax; then
        insinto /usr/share/vim/vimfiles/syntax
        doins samples/bip.vim
        insinto /usr/share/vim/vimfiles/ftdetect
        doins "${FILES}"/bip.vim
    fi
}
